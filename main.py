from headliner.trainer import Trainer
from headliner.model.transformer_summarizer import TransformerSummarizer
from headliner.model.bert_summarizer import BertSummarizer
from headliner.evaluation import BleuScorer
import dsgen
from data_loader import glove_embeddings
from headliner.preprocessing.preprocessor import Preprocessor
import os
try:
   import cPickle as pickle
except:
   import pickle

# TODO use BertSummarizer because it its pre-trained => could get better results

# TODO probably other preprocessing steps

"""
Can train with Transformer or a pre-trained BERT with Transformer on top
Pass "transformer" or "bert" as model name to decide which one to use
"""
class HeadlinerNetwork:
    def __init__(self, save_name='test', model_name="transformer", path="./data/subtitles", csv_path="./data/metadata/DEvideos.csv", batch_size=128, steps_per_epoch=100, dropout_rate=0.25, continue_from_model=None):
        self.save_name = save_name
        if model_name == "bert":
            if continue_from_model == None:
                self.summarizer = BertSummarizer(num_heads=8,
                                        feed_forward_dim=1024,
                                        num_layers_encoder=4,
                                        num_layers_decoder=4,
                                        bert_embedding_encoder='bert-base-uncased',
                                        embedding_encoder_trainable=False,
                                        embedding_size_encoder=768,
                                        embedding_size_decoder=768,
                                        dropout_rate=0.5,
                                        max_prediction_len=10)
                
                # TODO could adjust the learning rate
               # self.summarizer.optimizer_decoder = BertSummarizer.new_optimizer_decoder(
            #learning_rate_start=0.001
            #    )
            
            else:
                # load existing model
               self.summarizer = BertSummarizer.load(continue_from_model) 

        elif model_name == "transformer":
            if continue_from_model == None:
                # youtube has a limit of 100 chars for titles, but everything >70 chars will be truncated, so most vids have <=70 chars
                self.summarizer = TransformerSummarizer(
                                embedding_size=256,
                                embedding_encoder_trainable=False,
                                embedding_decoder_trainable=False,
                                dropout_rate=dropout_rate,
                                max_prediction_len=10)
                
                # sadly there is no learning rate param for transformer..
                # self.summarizer.new_optimizer()

            else:
                # load existing model
                self.summarizer = TransformerSummarizer.load(continue_from_model)

 
        preprocessor = Preprocessor(
                            lower_case=True,
                            hash_numbers=True,
                            start_token='<start>',
                            end_token='<end>',
                            punctuation_pattern='([!.?,])',
                            filter_pattern='(["#$%&()*+/:;<=>@[\\]^_`{|}~\t\n])',
                            add_input_start_end=True)
                            
        self.trainer = Trainer(
                        batch_size=batch_size,
                        steps_per_epoch=steps_per_epoch,
                        tensorboard_dir=f'./outputs/tmp/tensorboard_{self.save_name}',
                        model_save_path=f'./outputs/tmp/summarizer_{self.save_name}',
                        num_print_predictions=5,
                        embedding_path_decoder='./data/glove/vocab.txt',
                        embedding_path_encoder='./data/glove/vectors.txt',
                        preprocessor=preprocessor
                        )


        self.trainds, self.testds = load_dataset(path, csv_path, model_name)


    def train(self, epochs=30):
        self.trainer.train(self.summarizer, self.trainds, num_epochs=epochs, val_data=self.testds)
        self.summarizer.save(f"./outputs/summarizer_{self.save_name}")
        return

    # advanced = true returns a dictionary including attention weights and logits
    # advanced = false returns only the predicted string
    def predict(self, text, target, advanced=False):
        if advanced:
            return self.summarizer.predict_vectors(input_text=text, target_text=target)
            
        else:
            return self.summarizer.predict(text)





def evaluate(model_path, test_set):
    model = HeadlinerNetwork(continue_from_model=model_path)
    total_score = 0
    scorer = BleuScorer((0.5, 0.25, 0.125, 0.125))
    # TODO we need val and test set, not only val
    # TODO prediction should be a dict, figure out how to get this
    for n, sample in enumerate(test_set):
        caption = sample[0]
        target = sample[1]
        prediction = model.predict(caption, target, advanced=True)
        print(f"target: {target}")
        print(f"prediction keys: {prediction.get('predicted_text'), ''} ")
        # TODO add weights for n_grams and maybe tokens_to_ignore => filler words
        score = scorer.__call__(prediction=prediction)
        print(f"score: {score}")

        # average score over all samples
        total_score = (total_score + score) / (n +1)
        print(f"current sample prediction score: {score}")
        print(f"current total prediction score: {score}")
    print("evaluation done")


def load_dataset(path=None, csv_path=None, model_name='transformer'):
    if not os.path.isfile(f"train_{model_name}.pickle") or not os.path.isfile(f"test_{model_name}.pickle"):
        return dsgen.DSGenerator(path, csv_path, model_name).create_dataset()

    else:
        with open(rf"train_{model_name}.pickle", "rb") as train_file:
            train = pickle.load(train_file)
        with open(rf"test_{model_name}.pickle", "rb") as test_file:
            test = pickle.load(test_file)
        return train, test


# downloads glove embeddings (if not saved yet)
# glove_embeddings.save_glove_embeddings()

# _, test_set = load_dataset()
# evaluate('./outputs/tmp/summarizer_lesswords', test_set)


# model = HeadlinerNetwork(continue_from_model='./outputs/summarizer_lesswords')
model = HeadlinerNetwork(model_name='bert', save_name='bert')
model.train(epochs=250)