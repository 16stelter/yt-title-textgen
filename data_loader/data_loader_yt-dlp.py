import os
import csv
import subprocess
import multiprocessing

metadata_file_path = "../data/metadata/DEvideos.csv"
language = "de"
subtitle_folder_path = "../data/subtitles/"
base_url = "https://www.youtube.com/watch?v="

"""
Reads the dataset file which can be found at https://www.kaggle.com/datasnaek/youtube-new?select=DEvideos.csv
and returns its content as list of dictionaries
"""
def read_metadata_file():
    with open(metadata_file_path, "r", encoding="utf8") as file:
        lines = list(csv.DictReader(file))
    return lines

def save_captions(video_id: str = ''):
    """
    Uses yt-dlp to download the video captions
    """
    output_file = f"{subtitle_folder_path}{video_id}"
    print(f"ID: {video_id}")
    if(len(video_id)) == 11:
        try:
            # for later re-runs, don't try to download twice
            if not os.path.isfile(f"{subtitle_folder_path}{video_id}.{language}.vtt"):
                command = f"yt-dlp {base_url}{video_id} -o {output_file} --write-subs --compat-options no-live-chat --no-download --sub-langs {language}"
                subprocess.check_call(command)

                # if tried to get the original captions but there were none, use the auto-generated ones
                # but mark them as such via filename to be able to distinguish them later
                if not os.path.isfile(f"{subtitle_folder_path}{video_id}.{language}.vtt") and not os.path.isfile(f"{subtitle_folder_path}{video_id}_autogen.{language}.vtt"):
                    command = f"yt-dlp {base_url}{video_id} -o {output_file}_autogen --write-subs --write-auto-subs --compat-options no-live-chat --no-download --sub-langs {language}"
                    subprocess.check_call(command)
        except subprocess.CalledProcessError:
            # might fail due to e.g. private video settings or removed videos
            # if this is the case, just ignore it
            pass
    else:
        print("invalid video_id")


if __name__ == "__main__":
    metadata = read_metadata_file()
    print(len(metadata))
    metadata = metadata[9000:]
    print(len(metadata))

    video_ids = [entry.get('video_id', '') for entry in metadata]
    print(f"running with cpu_count = {os.cpu_count() -5}")
    pool = multiprocessing.Pool(os.cpu_count() -5)
    pool.map(save_captions, video_ids)
    save_captions()
    pool.close()