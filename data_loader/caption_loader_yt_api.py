import io
import os

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors

from googleapiclient.http import MediaIoBaseDownload


"""
This file is now deprecated since using the offical yt api had too many drawbacks.
Its currently kept for reference anyways.

-> 10k points / day
-> request to get subtitle id (50p) + request to download subtitle (200p) -> only 40 per day
-> auth entry for each video manually
"""

scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]

os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

api_service_name = "youtube"
api_version = "v3"
client_secrets_file = "client_secret_desktop.json"


"""
Get credentials and create an API client
"""
def init_api_client():
   flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
       client_secrets_file, scopes)
   credentials = flow.run_console()
   youtube = googleapiclient.discovery.build(api_service_name, api_version, credentials=credentials)

   return youtube

"""
Each request to this api endpoint costs 200 points.
The default limit per project is 10k points per day.

Decided to use in SubRip subtitle (srt) format because its relatively simple to parse
and contains valuable information (part_number, timeframe, text)
"""
def save_captions(video_id: str):
    youtube = init_api_client()
    request = youtube.captions().download(
        id=video_id,
    )
    fh = io.FileIO(f"./data/{video_id}.txt", "wb")

    download = MediaIoBaseDownload(fh, request)
    complete = False
    while not complete:
      status, complete = download.next_chunk()
      print(status)


save_captions("hSrIKpKB9rx1PW23Vl7yCPDx6_BG_NJp")