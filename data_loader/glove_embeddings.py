import os
from urllib.request import urlretrieve

glove_embeddings_path = "../data/glove/"

"""
To prevent too much savespace usage on git, load pre-trained glove embeddings by yourself :)

Glove embeddings could also be trained on our own dataset, e.g. by using https://gitlab.com/deepset-ai/open-source/glove-embeddings-de
"""
def save_glove_embeddings():
    print("checking the presence of pre-trained glove embeddings...")
    base_url = "https://int-emb-glove-de-wiki.s3.eu-central-1.amazonaws.com/"
    if not os.path.isfile(f"{glove_embeddings_path}/vocab.txt"):
        print("downloading vocab.txt, pls wait...")
        file_name = "vocab.txt"
        download(f"{base_url}{file_name}", file_name)

    if not os.path.isfile(f"{glove_embeddings_path}/vectors.txt"):
        print("downloading vectors.txt, pls wait...")
        file_name = "vectors.txt"
        download(f"{base_url}{file_name}", file_name)
    print("done")
    

def download(url, file_name):
    # urlretrieve is able to manage big files, as opposed to e.g. the standard requests module
    urlretrieve(url, f'{glove_embeddings_path}{file_name}')


#if __name__ == '__main__':
#    save_glove_embeddings()
